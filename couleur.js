// ==============================
//       CANVAS 1
// ==============================

modeCouleur1 = 'screen'

canvas1 = document.getElementById("canvas1");
stage1 = new createjs.Stage(canvas1);
stage1.enableMouseOver();
createjs.Touch.enable(stage1);
stage1.mouseMoveOutside = true;

posR = 150;
posV = 250;
posB = 350;

// ===== FOND =====

fond1Noir = new createjs.Shape();
stage1.addChild(fond1Noir);
fond1Noir.graphics.f('#000').rr(10, 10, 480, 290, 40).ef();

// ===== HALOS =====

haloRouge = new createjs.Shape();
stage1.addChild(haloRouge);
haloRouge.compositeOperation = modeCouleur1;
haloRouge.cursor = 'pointer';
haloRouge.x = 100;
haloRouge.y = 150;
haloRouge.graphics.rf(['#FF0000','#FF000000'],[0.94,1],0,0,0,0,0,71).dc(0,0,70);

haloVert = new createjs.Shape();
stage1.addChild(haloVert);
haloVert.compositeOperation = modeCouleur1;
haloVert.cursor = 'pointer';
haloVert.x = 250;
haloVert.y = 100;
haloVert.graphics.rf(['#00FF00','#00FF0000'],[0.94,1],0,0,0,0,0,71).dc(0,0,70);

haloBleu = new createjs.Shape();
stage1.addChild(haloBleu);
haloBleu.compositeOperation = modeCouleur1;
haloBleu.cursor = 'pointer';
haloBleu.x = 400;
haloBleu.y = 150
haloBleu.graphics.rf(['#0000FF','#0000FF00'],[0.94,1],0,0,0,0,0,71).dc(0,0,70);

var selectionSpot;
var faisceauAModifier;

function onMouseDownHalo(evt) {
    obj = evt.currentTarget;
    if (obj.id == 2) {
        selectionSpot = spotRouge;
        faisceauAModifier = faisceauRouge;
    }
    if (obj.id == 3) {
        selectionSpot = spotVert;
        faisceauAModifier = faisceauVert;
    }
    if (obj.id == 4) {
        selectionSpot = spotBleu;
        faisceauAModifier = faisceauBleu;
    }
    //
    obj.offsetX = obj.x - evt.stageX;
    obj.offsetY = obj.y - evt.stageY;
}
function onPressMoveHalo(evt) {
    obj.x = Math.min(Math.max(100, evt.stageX + obj.offsetX), 400);
    obj.y = Math.min(Math.max(100, evt.stageY + obj.offsetY), 200);
    //
    angle = Math.atan2(obj.y - selectionSpot.y, obj.x - selectionSpot.x)
    selectionSpot.rotation = angle * 180 / 3.1415 + 90;
    faisceauAModifier.rotation = angle * 180 / 3.1415 + 90;
    stage1.update();
}

function onPressUpHalo(evt) {
    obj = evt.currentTarget;
}

haloRouge.addEventListener("mousedown", onMouseDownHalo);
haloRouge.addEventListener("pressmove", onPressMoveHalo);

haloVert.addEventListener("mousedown", onMouseDownHalo);
haloVert.addEventListener("pressmove", onPressMoveHalo);

haloBleu.addEventListener("mousedown", onMouseDownHalo);
haloBleu.addEventListener("pressmove", onPressMoveHalo);

// ===== Faisceaux =====

faisceauRouge = new createjs.Shape();
stage1.addChild(faisceauRouge);
faisceauRouge.graphics.rf(['#990000FF', '#99000000'], [0, 1], 0, -30, 0, 0, -30, 35).mt(-18, -30).lt(-35, -80).lt(35, -80).lt(18, -30).ef();

faisceauVert = new createjs.Shape();
stage1.addChild(faisceauVert);
faisceauVert.graphics.rf(['#009900FF', '#00990000'], [0, 1], 0, -30, 0, 0, -30, 35).mt(-18, -30).lt(-35, -80).lt(35, -80).lt(18, -30).ef();

faisceauBleu = new createjs.Shape();
stage1.addChild(faisceauBleu);
faisceauBleu.graphics.rf(['#000099FF', '#00009900'], [0, 1], 0, -30, 0, 0, -30, 35).mt(-18, -30).lt(-35, -80).lt(35, -80).lt(18, -30).ef();


// ===== SPOTS =====
//
spotRouge = new createjs.Shape();
stage1.addChild(spotRouge);
spotRouge.graphics.f('#333').dr(-20, -30, 40, 60);
spotRouge.graphics.f('#666').dr(-25, -30, 5, 60);
spotRouge.graphics.f('#666').dr(20, -30, 5, 60).ef();
spotRouge.graphics.ss(0.5, 1).s('#000').dr(-25, -30, 50, 60).es();
spotRouge.graphics.f('#333').mt(-18, -30).lt(-21, -38).lt(21, -38).lt(18, -30).ef();
spotRouge.x = posR;
spotRouge.y = 310;
spotRouge.rotation = Math.atan2(haloRouge.y-spotRouge.y, haloRouge.x-spotRouge.x) * 180 / Math.PI + 90;

faisceauRouge.x = spotRouge.x;
faisceauRouge.y = spotRouge.y;
faisceauRouge.rotation = Math.atan2(haloRouge.y-spotRouge.y, haloRouge.x-spotRouge.x) * 180 / Math.PI + 90;

//
spotVert = new createjs.Shape();
stage1.addChild(spotVert);
spotVert.graphics.f('#333').dr(-20, -30, 40, 60);
spotVert.graphics.f('#666').dr(-25, -30, 5, 60);
spotVert.graphics.f('#666').dr(20, -30, 5, 60).ef();
spotVert.graphics.ss(0.5, 1).s('#000').dr(-25, -30, 50, 60).es();
spotVert.graphics.f('#333').mt(-18, -30).lt(-21, -38).lt(21, -38).lt(18, -30).ef();
spotVert.x = posV;
spotVert.y = 310;
spotVert.rotation = 0;

faisceauVert.x = spotVert.x;
faisceauVert.y = spotVert.y;
faisceauVert.rotation = Math.atan2(haloVert.y-spotVert.y, haloVert.x-spotVert.x) * 180 / Math.PI + 90;

//
spotBleu = new createjs.Shape();
stage1.addChild(spotBleu);
spotBleu.graphics.f('#333').dr(-20, -30, 40, 60);
spotBleu.graphics.f('#666').dr(-25, -30, 5, 60);
spotBleu.graphics.f('#666').dr(20, -30, 5, 60).ef();
spotBleu.graphics.ss(0.5, 1).s('#000').dr(-25, -30, 50, 60).es();
spotBleu.graphics.f('#333').mt(-18, -30).lt(-21, -38).lt(21, -38).lt(18, -30).ef();
spotBleu.x = posB;
spotBleu.y = 310;
spotBleu.rotation = Math.atan2(haloBleu.y-spotBleu.y, haloBleu.x-spotBleu.x) * 180 / Math.PI + 90;

faisceauBleu.x = spotBleu.x;
faisceauBleu.y = spotBleu.y;
faisceauBleu.rotation = Math.atan2(haloBleu.y-spotBleu.y, haloBleu.x-spotBleu.x) * 180 / Math.PI + 90;

// ===== CURSEURS =====

var selectionHalo;

function onMouseDownCurseur(evt) {
    obj = evt.currentTarget;
    //
    if (obj.id == 13) {
        selectionHalo = haloRouge;
        faisceauAModifier = faisceauRouge;
    }
    if (obj.id == 14) {
        selectionHalo = haloVert;
        faisceauAModifier = faisceauVert;
    }
    if (obj.id == 15) {
        selectionHalo = haloBleu;
        faisceauAModifier = faisceauBleu;
    }
    //
    obj.offsetY = obj.y - evt.stageY;
}
function onPressMoveCurseur(evt) {
    obj.y = Math.min(Math.max(380, evt.stageY + obj.offsetY), 480);
    //
    selectionHalo.alpha = (100 - obj.y + 380) / 100;
    faisceauAModifier.alpha = (100 - obj.y + 380) / 100;
    stage1.update();
}

function onPressUpCurseur(evt) {
    obj = evt.currentTarget;
}

fond1Barres = new createjs.Shape();
stage1.addChild(fond1Barres)
fond1Barres.graphics.ss(1,1,1).s('#000').f('#666').rr(posR-4,380,8,100,3);
fond1Barres.graphics.ss(3).s('#444').mt(posR,384).lt(posR, 476);
fond1Barres.graphics.ss(1,1,1).s('#000').f('#666').rr(posV-4,380,8,100,3);
fond1Barres.graphics.ss(3).s('#444').mt(posV,384).lt(posV, 476);
fond1Barres.graphics.ss(1,1,1).s('#000').f('#666').rr(posB-4,380,8,100,3);
fond1Barres.graphics.ss(3).s('#444').mt(posB,384).lt(posB, 476);

levelBarres = new createjs.Shape();
stage1.addChild(levelBarres);
levelBarres.graphics.sd([20, 10], 0).s('#444').mt(100,476).lt(400,476);
levelBarres.graphics.sd([20, 10], 0).s('#444').mt(100,430).lt(400,430);
levelBarres.graphics.sd([20, 10], 0).s('#444').mt(100,384).lt(400,384);

//
curseurRouge = new createjs.Shape();
stage1.addChild(curseurRouge);
curseurRouge.cursor = 'pointer'
curseurRouge.graphics.s(1).ss('#000').f('#F00').dc(0, 0, 9).ef().es();
curseurRouge.x = posR;
curseurRouge.y = 380;
curseurRouge.shadow = new createjs.Shadow('#333',2,2,3)

curseurRouge.addEventListener("mousedown", onMouseDownCurseur);
curseurRouge.addEventListener("pressmove", onPressMoveCurseur);

//
curseurVert = new createjs.Shape();
stage1.addChild(curseurVert);
curseurVert.cursor = 'pointer'
curseurVert.graphics.s(1).ss('#000').f('#0F0').dc(0, 0, 9).ef().es();
curseurVert.x = posV;
curseurVert.y = 380;
curseurVert.shadow = new createjs.Shadow('#333',2,2,3)

curseurVert.addEventListener("mousedown", onMouseDownCurseur);
curseurVert.addEventListener("pressmove", onPressMoveCurseur);

//
curseurBleu= new createjs.Shape();
stage1.addChild(curseurBleu);
curseurBleu.cursor = 'pointer'
curseurBleu.graphics.s(1).ss('#000').f('#00F').dc(0, 0, 9).ef().es();
curseurBleu.x = posB;
curseurBleu.y = 380;
curseurBleu.shadow = new createjs.Shadow('#333',2,2,3)

curseurBleu.addEventListener("mousedown", onMouseDownCurseur);
curseurBleu.addEventListener("pressmove", onPressMoveCurseur);

var text0 = new createjs.Text("0%", "20px Arial", "#666");
text0.x = 80;
text0.y = 464;
text0.cursor = 'pointer';
text0.textAlign = 'right';
stage1.addChild(text0);

var text50 = new createjs.Text("50%", "20px Arial", "#666");
text50.x = 80;
text50.y = 416;
text50.cursor = 'pointer';
text50.textAlign = 'right';
stage1.addChild(text50);

var text100 = new createjs.Text("100%", "20px Arial", "#666");
text100.x = 80;
text100.y = 370;
text100.cursor = 'pointer';
text100.textAlign = 'right';
stage1.addChild(text100);

let interrupteurState = true;

function onMouseDownSwitch(evt){
  if (interrupteurState){
    text0.text = "0%";
    text50.text = "50%";
    text100.text = "100%";
  } else {
    text0.text = "0";
    text50.text = "127";
    text100.text = "255";
  }
  interrupteurState = !interrupteurState;
  stage1.update();
}

text0.addEventListener("click", onMouseDownSwitch);
text50.addEventListener("click", onMouseDownSwitch);
text100.addEventListener("click", onMouseDownSwitch);
// ==========================
//      INITIALISATION
// ==========================

var context = new AudioContext()
stage1.update();
